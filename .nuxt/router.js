import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _9a1b33e2 = () => interopDefault(import('../pages/companies.vue' /* webpackChunkName: "pages/companies" */))
const _ab989718 = () => interopDefault(import('../pages/company/index.vue' /* webpackChunkName: "pages/company/index" */))
const _0960ea9d = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _700218c2 = () => interopDefault(import('../pages/company/new.vue' /* webpackChunkName: "pages/company/new" */))
const _3fb94786 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/companies",
    component: _9a1b33e2,
    name: "companies"
  }, {
    path: "/company",
    component: _ab989718,
    name: "company"
  }, {
    path: "/login",
    component: _0960ea9d,
    name: "login"
  }, {
    path: "/company/new",
    component: _700218c2,
    name: "company-new"
  }, {
    path: "/",
    component: _3fb94786,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
